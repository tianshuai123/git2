/*
SQLyog Enterprise v12.09 (64 bit)
MySQL - 5.5.40 : Database - pk
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pk` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `pk`;

/*Table structure for table `customer` */

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `name` varchar(20) DEFAULT NULL,
  `idnumber` varchar(20) DEFAULT NULL,
  `roomnumber` int(11) DEFAULT NULL,
  `come_time` datetime DEFAULT NULL,
  `away_time` datetime DEFAULT NULL,
  `staystatus` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `customer` */

insert  into `customer`(`name`,`idnumber`,`roomnumber`,`come_time`,`away_time`,`staystatus`) values ('张三','654321190067894321',101,'2019-08-22 08:45:30','2019-08-28 08:45:37',0);

/*Table structure for table `room` */

DROP TABLE IF EXISTS `room`;

CREATE TABLE `room` (
  `roomnumber` int(11) DEFAULT NULL,
  `roomtype` varchar(20) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `roompic` varchar(200) DEFAULT NULL,
  `roombigpic` varchar(200) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `flag` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `room` */

insert  into `room`(`roomnumber`,`roomtype`,`price`,`description`,`roompic`,`roombigpic`,`status`,`flag`) values (101,'大床房',200,'宽敞大床房,欢迎入住','image/timgsmall.jpg','image/timg.jpg',1,1);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `name` varchar(20) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`name`,`username`,`password`,`status`) values ('admin','15798757833','admin',1),('lisi','139737836892','opiu',0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
