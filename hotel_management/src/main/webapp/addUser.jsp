<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <!-- 使用Edge最新的浏览器的渲染方式 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- viewport视口：网页可以根据设置的宽度自动进行适配，在浏览器的内部虚拟一个容器，容器的宽度与设备的宽度相同。
    width: 默认宽度与设备的宽度相同
    initial-scale: 初始的缩放比，为1:1 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->

    <title>分配user</title>
    <!-- 1. 导入CSS的全局样式 -->
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <!-- 2. jQuery导入，建议使用1.9以上的版本 -->
    <script src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
    <!-- 3. 导入bootstrap的js文件 -->
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

</head>

<%--border="1" cellspacing="0"--%>
<style type="text/css">
    td, th {
        text-align: center;
    }

    #div {
        width: 95%;
        margin-top: 30px;

    }

    #h1 {
        margin-bottom: 30px;
    }
    #save{
        background-color: #0078D7;
        height: 90px;

    }
    #save_a{
        float: right;
        margin:25px 30px;
    }
    #tu_div{
        float: left;
        margin:20px 50px;
    }
    #span{
       float: right;
        margin:  30px 30px;
    }
    nav{
        float: right;
        margin-right: 550px;
    }

    #id_save{
        background-color: #a6e1ec;
        margin-top: 150px;
    }
    tr{
        height: 30px;
    }
</style>

<div id="save">
    <div id="tu_div" color="#f8f8ff"><button type="button" class="btn btn-success" onclick="butto()">返回首页</button></div>
    <a href="javascript:void(0)" class="btn btn-default" role="button" id="save_a">新增</a>
    <%--<a href="${pageContext.request.contextPath}/addUser.jsp" class="btn btn-default" role="button" id="save_a">新增</a>--%>
</div>
<center>
    <div id="div">
        <h1 id="h1">分配角色页面</h1>
        <div id="tablediv">
        <table id="table" border="1" cellspacing="0"  class="table table-hover table-bordered">

            <tr class="active" >
                <th>name</th>
                <th>username</th>
                <th>password</th>
                <th>操作</th>
            </tr>
        </table>
    </div>
        <nav>
            <ul class="pagination" id="fenye">
                <%--分页内容填充--%>
            </ul>
            <span id="span"></span>

        </nav>

    </div>

</center>


<script>
function butto(){
    location.href='${pageContext.request.contextPath}/home.jsp';
}
    $(function () {

        findall(1);
        $("#id_save").hide();
    });
    function findall(pagenum) {
        $.post("${pageContext.request.contextPath}/findAll", {pagenum:pagenum}, function (page) {

            var tt = '';

            var t='<tr class="active">\n' +
            '                <th>name</th>\n' +
            '                <th>username</th>\n' +
            '                <th>password</th>\n' +
            '                <th>操作</th>\n' +
            '            </tr>';
         tt+=t;
            for (var i = 0; i < page.list.length; i++) {

                var td = ' <tr>\n' +
                    '            <td>' + page.list[i].name + '</td>\n' +
                    '            <td>' + page.list[i].username + '</td>\n' +
                    '            <td>' + page.list[i].password + '</td>\n' +
                    '             <td><span onclick="javascript:deleteUser(\'' + page.list[i].name + '\')"><a href="javascript:void(0)">删除</a></span></td>\n' +
                    '            </tr>';
                tt = tt + td;

            }

         /*   var table = $("#table").html();
            table+=tt;
            $("#table").html(table);
*/
            $("#table").html(tt);
            $("#table").css("background-color","#a6e1ec");



            var li='';
            var sh=page.prePage;
            if (sh<1) {
                sh=1;
            }
            li+='<li>\n' +
                '                    <a href="javascript:findall('+sh+')" aria-label="Previous">\n' +
                '                        <span aria-hidden="true">&laquo;</span>\n' +
                '                    </a>\n' +
                '                </li>';

            for (var i = 1; i<= page.pages; i++) {
                var yema='';
                if (page.pageNum==i){
                    yema='<li  class="active"><a href="javascript:findall('+i+')">'+i+'</a></li>';
                }
                yema='<li><a href="javascript:findall('+i+')">'+i+'</a></li>';
                li+=yema;
            }
            var x=page.pageNum+1;
            if (x>page.lastPage) {
                x=page.lastPage;
            }
            li+=' <li>\n' +
                '                    <a href="javascript:findall('+x+')" aria-label="Next">\n' +
                '                        <span aria-hidden="true">&raquo;</span>\n' +
                '                    </a>\n' +
                '                </li>';

            $("#fenye").html(li);

            var tiaomu='共'+page.total+'条,有'+page.pages+'页';
            $("#span").html(tiaomu);
        });
    }

    function deleteUser(name) {

        var flag = window.confirm("确定删除吗?");

        if (flag) {
            $.post("${pageContext.request.contextPath}/deleteUser", {name: name}, function () {
                location.reload(true);
            })
        }
    }


    $("#save_a").click(saveuser);

    function saveuser() {
        $("#id_save").toggle(1000);
    }
</script>

<body>







<div id="id_save">
    <script>
        $(function () {
            $("#name").blur(name);
            $("#username").blur(username);
            $("#password").blur(password1);
            $("#password2").blur(password2);

            $("#button").click(function () {
                var name_span = $("#namespan").text();
                var name_ok = name_span == "ok" ? true : false;

                var username_span = $("#usernamespan").text();
                var username_ok = username_span == "ok" ? true : false;

                if (name_ok && username_ok && password1() && password2()) {
                    var name_value = $("#name").val();
                    var usernaem = $("#username").val();
                    var password = $("#password2").val();
                    $.post("${pageContext.request.contextPath}/saveUser", {
                        name: name_value,
                        username: usernaem,
                        password: password
                    }, function () {
                        <%--location.href = "${pageContext.request.contextPath}/allotUser.jsp"--%>
                        location.reload(true);
                    });
                } else {
                    alert("格式不正确，不能提交")
                }
            });

        });

        function name() {

            var name_value = $("#name").val();

            if (name_value == null || name_value.length == 0) {
                $("#namespan").css("color", "red");
                $("#namespan").html("invalid");

            } else {
                $.post("${pageContext.request.contextPath}/findName", {name: name_value}, function (user) {

                    if (user.name == null) {
                        $("#namespan").css("color", "green");
                        $("#namespan").text("ok");

                    }
                    else {
                        $("#namespan").css("color", "red");
                        $("#namespan").text("repeat");

                    }
                });
            }

        }

        function username() {
            var username_value = $("#username").val();
            if (username_value) {
                $.post("${pageContext.request.contextPath}/findUserName", {username: username_value}, function (user) {
                    if (user.username == null) {
                        $("#usernamespan").css("color", "green");
                        $("#usernamespan").html("ok");
                    }
                    else {
                        $("#usernamespan").css("color", "red");
                        $("#usernamespan").html("repeat");
                    }
                });

            } else {
                var css = $("#usernamespan").css("color", "red");
                $("#usernamespan").html("invalid");
            }

        }

        function password1() {
            var password_value = $("#password").val();
            var reg = /^\w{6,12}$/;
            var flag = reg.test(password_value);
            if (flag) {
                $("#passwordspan1").css("color", "green");
                $("#passwordspan1").html("ok");
                return true;
            } else {
                $("#passwordspan1").css("color", "red");
                $("#passwordspan1").html("invalid");
                return false;
            }
        }

        function password2() {
            var password1_value = $("#password").val();
            var password2_value = $("#password2").val();
            var flag = password1_value == password2_value ? true : false;
            if (flag) {
                $("#passwordspan2").css("color", "green");
                $("#passwordspan2").html("ok");
                return true;
            } else {
                $("#passwordspan2").css("color", "red");
                $("#passwordspan2").html("invalid");
                return false;
            }
        }
    </script>
    <style>
        #name, #username, #password, #password2 {
            width: 500px;
            margin-top: 10px;
        }

        label {
            margin-right: 20px;
            margin-top: 10px;
        }

        #button {
            /* margin-top: 30px;*/
            margin:30px 80px
        }
        span{
            /* margin-right:30px;*/
            float: left;
        }
       /* #id_save{

            background-color: #a6e1ec;
            margin-top: 50px;
        }*/
    </style>



    <!--注册表单-->
    <center>
        <form id="form">


            <table style="margin-top: 25px;">
                <tr>
                    <td class="td_left">
                        <label for="name">姓名</label>
                    </td>
                    <td class="td_right">
                        <input type="text" class="form-control" id="name" name="name" placeholder="请输入真实姓名">
                    </td>
                    <td><span id="namespan"></span></td>
                </tr>
                <tr>
                    <td class="td_left">
                        <label for="username">用户名</label>
                    </td>
                    <td class="td_right">
                        <input type="text" class="form-control" id="username" name="username" placeholder="请输用户名">
                    </td>
                    <td><span id="usernamespan"></span></td>
                </tr>
                <tr>
                    <td class="td_left">
                        <label for="password">密码</label>
                    </td>
                    <td class="td_right">
                        <input type="text" class="form-control" id="password" name="password" placeholder="请输入密码">
                    </td>
                    <td><span id="passwordspan1"></span></td>
                </tr>
                <tr>
                    <td class="td_left">
                        <label for="password2">请确认密码</label>
                    </td>
                    <td class="td_right">
                        <input type="text" class="form-control" id="password2" name="password2" placeholder="请再次输入密码">
                    </td>
                    <td><span id="passwordspan2"></span></td>
                </tr>


                <tr>

                    <td class="td_left">
                    </td>
                    <td class="td_right check">
                        <%--<input type="button"class="" value="添加" >--%>
                        <button type="button" class="btn btn-default submit " id="button">添 加</button>
                    </td>

                </tr>
            </table>
        </form>
    </center>
</div>
</body>
</html>
