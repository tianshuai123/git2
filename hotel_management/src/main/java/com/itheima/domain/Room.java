package com.itheima.domain;

public class Room {
    private int roomNumber;//房间号
    private String roomType;//房间类型
    private int price;//房间价格
    private String description;//描述
    private String roomPic;//房间缩略图
    private String roomBigPic;//房间大图
    private int status;//入住状态,空还是满
    private int flag;//房间的删除状态,1代表存在,0代表删除

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRoomPic() {
        return roomPic;
    }

    public void setRoomPic(String roomPic) {
        this.roomPic = roomPic;
    }

    public String getRoomBigPic() {
        return roomBigPic;
    }

    public void setRoomBigPic(String roomBigPic) {
        this.roomBigPic = roomBigPic;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "Room{" +
                "roomNumber=" + roomNumber +
                ", roomType='" + roomType + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                ", roomPic='" + roomPic + '\'' +
                ", roomBigPic='" + roomBigPic + '\'' +
                ", status=" + status +
                ", flag=" + flag +
                '}';
    }
}
