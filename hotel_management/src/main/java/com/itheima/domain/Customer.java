package com.itheima.domain;

import java.util.Date;

public class Customer {
    private String name;//客户姓名
    private String idNumber;//客户身份证号
    private int roomNumber;//入住房间号
    private Date come_time;//入住时间
    private Date away_time;//退房时间
    private int stayStatus;//入住状态,已退房还是入住中

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Date getCome_time() {
        return come_time;
    }

    public void setCome_time(Date come_time) {
        this.come_time = come_time;
    }

    public Date getAway_time() {
        return away_time;
    }

    public void setAway_time(Date away_time) {
        this.away_time = away_time;
    }

    public int getStayStatus() {
        return stayStatus;
    }

    public void setStayStatus(int stayStatus) {
        this.stayStatus = stayStatus;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", idNumber='" + idNumber + '\'' +
                ", roomNumber=" + roomNumber +
                ", come_time=" + come_time +
                ", away_time=" + away_time +
                ", stayStatus=" + stayStatus +
                '}';
    }
}
