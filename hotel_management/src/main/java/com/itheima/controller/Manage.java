package com.itheima.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.domain.User;
import com.itheima.service.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class Manage {
    @Autowired
    private UserService userService;

    @RequestMapping("/test")
    public String test(Model model) {
        System.out.println("pk项目测试");
        model.addAttribute("test", "helloworld");
        return "login";
    }

   /* @RequestMapping("/findAll")
    @ResponseBody
    public List<User> findAll( ) {

        List<User> list = userService.findAll();

        return list;
    }*/
   @RequestMapping("/findAll")
   @ResponseBody
   public   PageInfo<User> findAll(@RequestParam(defaultValue = "1")Integer pagenum ,@RequestParam(defaultValue = "5")Integer pagesize ) {
       Page<User> page = PageHelper.startPage(pagenum, pagesize);
       List<User> list = userService.findAll();
       PageInfo<User> userPageInfo = page.toPageInfo();

       return userPageInfo;
   }

    @RequestMapping("/deleteUser")
    @ResponseBody
    public void deleteUsername(String name) {

        userService.deletename(name);
    }
    @RequestMapping("/findName")
    @ResponseBody
    public User findName( String name) {

        User user=userService.findName(name);

       return user;
    }
    @RequestMapping("/findUserName")
    @ResponseBody
    public User findUserName(String username) {

        User user=userService.findUserName(username);

        return user;
    }
    @RequestMapping("/saveUser")
    @ResponseBody
    public void saveUser(User user) {

        userService.insertUser(user);

    }
}