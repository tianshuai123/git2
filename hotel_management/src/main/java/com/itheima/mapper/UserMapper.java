package com.itheima.mapper;

import com.itheima.domain.User;

import java.util.List;


public interface UserMapper {
    public List<User> findAll();

    void deletenem(String name);

    User findName(String name);

    User findUserName(String username);

    void insertUser(User user);
}
