package com.itheima.service;

import com.itheima.domain.User;

import java.util.List;

public interface UserService {
    public List<User> findAll();

    void deletename(String name);

    User findName(String name);

    User findUserName(String username);

    void insertUser(User user);
}
