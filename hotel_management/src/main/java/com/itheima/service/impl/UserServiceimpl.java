package com.itheima.service.impl;

import com.itheima.domain.User;
import com.itheima.mapper.UserMapper;
import com.itheima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserServiceimpl implements UserService {
    @Autowired
private UserMapper mapper;
    @Override
    public List<User> findAll(){
     return   mapper.findAll();
    }

    @Override
    public void deletename(String name) {
        mapper.deletenem(name);
    }

    @Override
    public User findName(String name) {

       User user= mapper. findName( name);
       return user;
    }

    @Override
    public User findUserName(String username) {

       return mapper.findUserName( username);
    }

    @Override
    public void insertUser(User user) {
        mapper.insertUser(user);
    }
}
